package modules

import (
	"gitlab.com/lear4212225/go-rpc-user/internal/infrastructure/component"
	uservice "gitlab.com/lear4212225/go-rpc-user/internal/modules/user/service"
	"gitlab.com/lear4212225/go-rpc-user/internal/storages"
)

type Services struct {
	User          uservice.Userer
	UserClientRPC uservice.Userer
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	return &Services{
		User: uservice.NewUserService(storages.User, components.Logger),
	}
}
