package grpc

import (
	"context"

	"gitlab.com/lear4212225/go-rpc-user/internal/models"

	"gitlab.com/lear4212225/go-rpc-user/internal/modules/user/service"
)

type GRPCUserService struct {
	userService service.Userer
}

func NewGRPCUserService(userService service.Userer) *GRPCUserService {
	return &GRPCUserService{userService: userService}
}

func (g *GRPCUserService) CreateUser(ctx context.Context, in *UserCreateIn) (*UserCreateOut, error) {
	inService := service.UserCreateIn{
		Name:     in.Name,
		Phone:    in.Phone,
		Email:    in.Email,
		Password: in.Password,
		Role:     int(in.Role),
	}
	out := g.userService.Create(ctx, inService)
	outGService := UserCreateOut{
		UserID:    int32(out.UserID),
		ErrorCode: int32(out.ErrorCode),
	}
	return &outGService, nil
}

func (g *GRPCUserService) Update(ctx context.Context, in *UserUpdateIn) (*UserUpdateOut, error) {
	inService := service.UserUpdateIn{
		User: models.User{
			ID:            int(in.User.ID),
			Name:          in.User.Name,
			Phone:         in.User.Phone,
			Email:         in.User.Email,
			Password:      in.User.Password,
			Role:          int(in.User.Role),
			Verified:      in.User.Verified,
			EmailVerified: in.User.EmailVerified,
			PhoneVerified: in.User.PhoneVerified,
		},
		Fields: in.Fields,
	}
	outService := g.userService.Update(ctx, inService)
	out := UserUpdateOut{
		Success:   outService.Success,
		ErrorCode: int32(outService.ErrorCode),
	}
	return &out, nil
}

func (g *GRPCUserService) VerifyEmail(ctx context.Context, in *UserVerifyEmailIn) (*UserUpdateOut, error) {
	inService := service.UserVerifyEmailIn{
		UserID: int(in.UserID),
	}
	outService := g.userService.VerifyEmail(ctx, inService)
	out := UserUpdateOut{
		Success:   outService.Success,
		ErrorCode: int32(outService.ErrorCode),
	}
	return &out, nil
}

func (g *GRPCUserService) ChangePassword(ctx context.Context, in *ChangePasswordIn) (*ChangePasswordOut, error) {
	inService := service.ChangePasswordIn{
		UserID:      int(in.UserID),
		OldPassword: in.OldPassword,
		NewPassword: in.NewPassword,
	}
	outService := g.userService.ChangePassword(ctx, inService)
	out := ChangePasswordOut{
		Success:   outService.Success,
		ErrorCode: int32(outService.ErrorCode),
	}
	return &out, nil
}

func (g *GRPCUserService) GetByEmail(ctx context.Context, in *GetByEmailIn) (*UserOut, error) {
	inService := service.GetByEmailIn{
		Email: in.Email,
	}
	outService := g.userService.GetByEmail(ctx, inService)
	out := UserOut{
		User: &User{
			ID:            int32(outService.User.ID),
			Name:          outService.User.Name,
			Phone:         outService.User.Phone,
			Email:         outService.User.Email,
			Password:      outService.User.Password,
			Role:          int32(outService.User.Role),
			Verified:      outService.User.Verified,
			EmailVerified: outService.User.EmailVerified,
			PhoneVerified: outService.User.PhoneVerified,
		},
		ErrorCode: int32(outService.ErrorCode),
	}
	return &out, nil
}

func (g *GRPCUserService) GetByPhone(ctx context.Context, in *GetByPhoneIn) (*UserOut, error) {
	inService := service.GetByPhoneIn{
		Phone: in.Phone,
	}
	outService := g.userService.GetByPhone(ctx, inService)
	out := UserOut{
		User: &User{
			ID:            int32(outService.User.ID),
			Name:          outService.User.Name,
			Phone:         outService.User.Phone,
			Email:         outService.User.Email,
			Password:      outService.User.Password,
			Role:          int32(outService.User.Role),
			Verified:      outService.User.Verified,
			EmailVerified: outService.User.EmailVerified,
			PhoneVerified: outService.User.PhoneVerified,
		},
		ErrorCode: int32(outService.ErrorCode),
	}
	return &out, nil
}

func (g *GRPCUserService) GetByID(ctx context.Context, in *GetByIDIn) (*UserOut, error) {
	inService := service.GetByIDIn{
		UserID: int(in.UserID),
	}
	outService := g.userService.GetByID(ctx, inService)
	out := UserOut{
		User: &User{
			ID:            int32(outService.User.ID),
			Name:          outService.User.Name,
			Phone:         outService.User.Phone,
			Email:         outService.User.Email,
			Password:      outService.User.Password,
			Role:          int32(outService.User.Role),
			Verified:      outService.User.Verified,
			EmailVerified: outService.User.EmailVerified,
			PhoneVerified: outService.User.PhoneVerified,
		},
		ErrorCode: int32(outService.ErrorCode),
	}
	return &out, nil
}

func (g *GRPCUserService) GetByIDs(ctx context.Context, in *GetByIDsIn) (*UsersOut, error) {
	userIDs := make([]int, len(in.UserIDs))
	for i := 0; i < len(in.UserIDs); i++ {
		userIDs[i] = int(in.UserIDs[i])
	}
	inService := service.GetByIDsIn{
		UserIDs: userIDs,
	}
	outService := g.userService.GetByIDs(ctx, inService)
	users := make([]*User, len(outService.User))
	for i := 0; i < len(outService.User); i++ {
		users[i] = &User{
			ID:            int32(outService.User[i].ID),
			Name:          outService.User[i].Name,
			Phone:         outService.User[i].Phone,
			Email:         outService.User[i].Email,
			Password:      outService.User[i].Password,
			Role:          int32(outService.User[i].Role),
			Verified:      outService.User[i].Verified,
			EmailVerified: outService.User[i].EmailVerified,
			PhoneVerified: outService.User[i].PhoneVerified,
		}
	}
	out := UsersOut{
		User:      users,
		ErrorCode: int32(outService.ErrorCode),
	}
	return &out, nil
}

func (g *GRPCUserService) mustEmbedUnimplementedUsersServer() {
	//TODO implement me
	panic("implement me")
}
